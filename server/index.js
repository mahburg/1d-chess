require('dotenv').config();
const Fastify = require('fastify');
const path = require('node:path');

const PORT = Number(process.env.SERVER_PORT) || 3000;
const fastify = Fastify({ logger: process.env.NODE_ENV === 'development' });

fastify.register(require('@fastify/static'), {
  root: path.join(__dirname, '../build'),
});

fastify.get('/', (request, reply) => {
  reply.sendFile('index.html');
});

fastify.listen({ port: PORT }, (err, address) => {
  if (err) throw err;
});
