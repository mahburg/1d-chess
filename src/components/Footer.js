import styled from 'styled-components';

function Footer() {
  return (
    <Wrapper>
      <div className="left">&copy;{new Date().getFullYear()} Lloyd Grubham</div>
      <div className="center">
        Creator:{' '}
        <a href="https://docpop.itch.io/1d-chess" className="credit">
          Doctor Popular's 1D Chess
        </a>
      </div>
      <div className="right"></div>
    </Wrapper>
  );
}

const Wrapper = styled.footer`
  display: grid;
  grid-template-columns: 150px 1fr 150px;
  place-items: center;
  width: 100%;
  height: 30px;
  padding: 0 12px;
  font-size: 14px;
  background-color: #333;
  color: #aaa;
  position: absolute;
  bottom: 0;

  .center {
    color: white;

    a {
      color: white;
    }
  }
`;

export default Footer;
