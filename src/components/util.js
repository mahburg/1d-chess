// TODO: check for checkmate
// TODO: check for stalemate

export const WHITE = 'w';
const BLACK = 'b';
const PAWN = 'p';
const KNIGHT = 'n';
const BISHOP = 'b';
const ROOK = 'r';
const QUEEN = 'q';
const KING = 'k';

export const initialGameState = () => {
  return [
    'wk',
    'wq',
    'wr',
    'wb',
    'wn',
    'wp',
    null,
    null,
    null,
    null,
    'bp',
    'bn',
    'bb',
    'br',
    'bq',
    'bk',
  ];
};

export const checkForCheck = (gameState) => {
  const whiteKingIndex = gameState.findIndex((sq) => sq === 'wk');
  const blackKingIndex = gameState.findIndex((sq) => sq === 'bk');

  const blackThreatenedSquares = getSquaresUnderAttackByColor(gameState, BLACK);
  const whiteThreatenedSquares = getSquaresUnderAttackByColor(gameState, WHITE);

  return {
    whiteInCheck: blackThreatenedSquares.includes(whiteKingIndex),
    blackInCheck: whiteThreatenedSquares.includes(blackKingIndex),
  };
};

// export const checkForCheckmate = (gameState) => {
//   const { whiteInCheck, blackInCheck } = checkForCheck(gameState);

//   if (whiteInCheck) {
//     const whitePieceIndices = gameState.map((p, i) =>
//       p[0] === WHITE ? i : null
//     );
//   } else if (blackInCheck) {
//   }

//   return false;
// };

export const getThreatenedSquares = (gameState, squareIndex, threat) => {
  if (squareIndex === null) return [];

  const { piece } = getPieceData(gameState[squareIndex]);

  let moves = [];
  switch (piece) {
    case PAWN:
      moves = getPawnMoves(gameState, squareIndex, threat);
      break;
    case KNIGHT:
      moves = getKnightMoves(squareIndex);
      break;
    case BISHOP:
      moves = getBishopMoves(gameState, squareIndex);
      break;
    case ROOK:
      moves = getRookMoves(gameState, squareIndex);
      break;
    case QUEEN:
      moves = getQueenMoves(gameState, squareIndex);
      break;
    case KING:
      moves = getKingMoves(squareIndex);
      break;
    default:
      break;
  }

  return filterByValidBoard(moves);
};

const getOtherColor = (color) => {
  return color === WHITE ? BLACK : WHITE;
};

const nextGameState = (gameState, start, end) => {
  const newGameState = gameState.slice();
  newGameState[end] = newGameState[start];
  newGameState[start] = null;

  return newGameState;
};

export const getAvailableMoves = (gameState, squareIndex) => {
  const { piece, color } = getPieceData(gameState[squareIndex]);
  let threatenedSquares = getThreatenedSquares(gameState, squareIndex);

  if (piece === 'k') {
    const checkSquares = getSquaresUnderAttackByColor(
      gameState,
      getOtherColor(color)
    );
    threatenedSquares = threatenedSquares.filter(
      (sq) => !checkSquares.includes(sq)
    );
  }

  threatenedSquares = filterOutSelfCheck(
    threatenedSquares,
    gameState,
    squareIndex,
    color
  );

  return filterExcludeSameTeam(threatenedSquares, gameState, color);
};

export const removeDuplicates = (arr) => [...new Set(arr)];

const sameTeamColor = (color, sq) => !!(sq && color === sq[0]);

export const getPieceData = (data) => {
  if (!data) return { color: null, piece: null };

  const color = data[0];
  const piece = data[1];

  return { color, piece };
};

// const sameTeamPiece = (piece1, piece2) =>
//   !!(piece1 && piece2 && piece1[0] === piece2[0]);

const inBoardBounds = (index) => index >= 0 && index < 16;

const filterExcludeSameTeam = (arr, gameState, color) =>
  arr.filter((i) => !sameTeamColor(color, gameState[i]));

const filterByValidBoard = (arr) => arr.filter((i) => i >= 0 && i < 16);

const filterOutSelfCheck = (arr, gameState, squareIndex, color) => {
  return arr.filter(
    (nextMove) =>
      !checkForCheck(nextGameState(gameState, squareIndex, nextMove))?.[
        color === WHITE ? 'whiteInCheck' : 'blackInCheck'
      ]
  );
};

const getSquaresUnderAttackByColor = (gameState, color) => {
  const attackedSquares = new Set();

  gameState.forEach((piece, sqIndex) => {
    const data = getPieceData(piece);
    if (color === data.color) {
      const availableMovesList = getThreatenedSquares(gameState, sqIndex, true);
      availableMovesList.forEach((move) => {
        attackedSquares.add(move);
      });
    }
  });

  return [...attackedSquares];
};

const getKingMoves = (index) => {
  const moves = [];
  moves.push(index + 1);
  moves.push(index - 1);

  return moves;
};
const getQueenMoves = (gameState, index) => {
  const moves = everyNMoves(gameState, index, 1).concat(
    everyNMoves(gameState, index, 2)
  );

  return moves;
};
const getRookMoves = (gameState, index) => {
  const moves = everyNMoves(gameState, index, 1);

  return moves;
};
const getBishopMoves = (gameState, index) => {
  const moves = everyNMoves(gameState, index, 2);

  return moves;
};

const getKnightMoves = (index) => {
  const moves = [];

  moves.push(index + 2);
  moves.push(index + 3);
  moves.push(index - 2);
  moves.push(index - 3);

  return moves;
};

const getPawnMoves = (gameState, index, threat) => {
  const moves = [];

  const { color } = getPieceData(gameState[index]);
  const sign = color === WHITE ? 1 : -1;
  const square = index + sign;

  if (
    getPieceData(gameState[square]).color !== color ||
    gameState[square] === null
  ) {
    moves.push(square);
  }

  if (!threat) {
    if (
      color === WHITE &&
      index === 5 &&
      gameState[6] === null &&
      gameState[7] === null
    ) {
      moves.push(7);
    }
    if (
      color === BLACK &&
      index === 10 &&
      gameState[9] === null &&
      gameState[8] === null
    ) {
      moves.push(8);
    }
  }

  return moves;
};

const everyNMoves = (gameState, index, base) => {
  const moves = [];

  let continuePositive = true;
  let continueNegative = true;

  for (let i = base; i < 16; i += base) {
    const posIndex = index + i;
    const negIndex = index - i;

    if (continuePositive && inBoardBounds(posIndex)) {
      if (gameState[posIndex]) {
        moves.push(posIndex);
        continuePositive = false;
      } else {
        moves.push(posIndex);
      }
    }

    if (continueNegative && inBoardBounds(negIndex)) {
      if (gameState[negIndex]) {
        moves.push(negIndex);
        continueNegative = false;
      } else {
        moves.push(negIndex);
      }
    }
  }

  return moves;
};
