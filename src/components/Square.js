import React from 'react';
import styled from 'styled-components';
import Piece from './Piece';

function Square({ pieceData, selected, highlighted, sx, ...rest }) {

  
  let className = 'square';
  if (selected) className += ' selected';
  if (highlighted) className += ' highlighted';

  if (!pieceData) return <div className={className} style={sx} {...rest} />;

  return (
    <SquareWrapper className={className} style={sx} {...rest}>
      {Piece && <Piece pieceData={pieceData} />}
    </SquareWrapper>
  );
}

const SquareWrapper = styled.div`
  display: grid;
  place-items: center;
  width: 100%;
  height: 100%;

  &:nth-child(odd) {
    background-color: #ddd;
  }
  &:nth-child(even) {
    background-color: rgb(100, 100, 250);
  }

  &.selected {
    background-color: red;
  }

  &.highlighted {
    background-color: yellow;
    border: solid 1px gray;
  }

  svg {
    transform: scale(2);
  }
`;

export default Square;
