import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import Square from './Square';

import {
  checkForCheck,
  // checkForCheckmate,
  getAvailableMoves,
  getPieceData,
  initialGameState,
  WHITE,
} from './util';

function Board() {
  const [history, setHistory] = useState([initialGameState()]);
  const [futureHistory, setFutureHistory] = useState([]);
  const [whitesTurn, setWhitesTurn] = useState(true);
  const [selectedSquareIndex, setSelectedSquareIndex] = useState();
  const [possibleMoves, setPossibleMoves] = useState([]);
  const [checkData, setCheckData] = useState({
    whiteInCheck: false,
    blackInCheck: false,
  });

  const boardState = history[history.length - 1];

  const setBoardState = (newState) => {
    const newHistoryItem = history.slice();
    newHistoryItem.push(newState);

    setHistory(newHistoryItem);
  };

  const handleSquareClick = (i) => {
    if (selectedSquareIndex === i) {
      setSelectedSquareIndex(null);
    } else if (possibleMoves.includes(i)) {
      const copyState = boardState.map((piece, index) => {
        if (index === i) return boardState[selectedSquareIndex];
        if (index === selectedSquareIndex) return null;

        return piece;
      });

      const { whiteInCheck, blackInCheck } = checkForCheck(copyState);

      const { color } = getPieceData(boardState[selectedSquareIndex]);

      if (
        (color === WHITE && !whiteInCheck) ||
        (color !== WHITE && !blackInCheck)
      ) {
        setBoardState(copyState);
        setFutureHistory([]);
        setSelectedSquareIndex(null);

        setWhitesTurn(!whitesTurn);
      }
    } else {
      if (whitesTurn === (getPieceData(boardState[i]).color === WHITE)) {
        setSelectedSquareIndex(i);
      }
    }
  };

  useEffect(() => {
    const availableMovesList =
      selectedSquareIndex >= 0
        ? getAvailableMoves(boardState, selectedSquareIndex)
        : [];

    setPossibleMoves(availableMovesList);
  }, [selectedSquareIndex, boardState]);

  useEffect(() => {
    // console.log('checkmate: ', checkForCheckmate)

    setCheckData(checkForCheck(boardState));
  }, [boardState]);

  const undo = () => {
    const histLen = history.length;
    if (histLen > 1) {
      const newFuture = futureHistory.slice();
      const newHistory = history.slice();

      newFuture.push(newHistory.pop());

      setHistory(newHistory);
      setFutureHistory(newFuture);
      setWhitesTurn(!whitesTurn);
    }
  };
  const redo = () => {
    const histLen = futureHistory.length;
    if (histLen > 0) {
      const newFuture = futureHistory.slice();
      const newHistory = history.slice();

      newHistory.push(newFuture.pop());

      setHistory(newHistory);
      setFutureHistory(newFuture);
      setWhitesTurn(!whitesTurn);
    }
  };

  const undoEnabled = history.length > 1;
  const redoEnabled = futureHistory.length > 0;

  const gridStylesMap = {
    6: { gridArea: '3 / 5 / 3 / 5' },
    7: { gridArea: '3 / 4 / 3 / 4' },
    8: { gridArea: '3 / 3 / 3 / 3' },
    9: { gridArea: '3 / 2 / 3 / 2' },
  };

  return (
    <PlayArea className="play-area">
      <InfoRow className="info">
        <button disabled={!undoEnabled} onClick={undo}>
          Undo
        </button>
        <TurnIndicator className={whitesTurn ? 'white' : ' black'}>
          <p>Turn: {whitesTurn ? 'White' : 'Black'}</p>
        </TurnIndicator>
        <div className="placename">
          {checkData.whiteInCheck ? (
            <CheckIndicator>
              <p>White In Check</p>
            </CheckIndicator>
          ) : (
            checkData.blackInCheck && (
              <CheckIndicator>
                <p>Black in Check</p>
              </CheckIndicator>
            )
          )}
        </div>
        <button disabled={!redoEnabled} onClick={redo}>
          Redo
        </button>
      </InfoRow>
      <BoardGrid
        className={
          checkData.whiteInCheck || checkData.blackInCheck ? 'check' : ''
        }
      >
        <div
          className="block"
          style={{ gridRow: '2 /span 4', gridColumn: 1 }}
        />
        <div
          className="block"
          style={{ gridRow: '2', gridColumn: '2 / span 3' }}
        />
        <div
          className="block"
          style={{ gridRow: '1 / span 4', gridColumn: '6' }}
        />
        <div
          className="block"
          style={{ gridRow: '4', gridColumn: '3 / span 3' }}
        />

        {boardState
          ? boardState.map((sq, i) => (
              <Square
                key={sq || i}
                onClick={() => handleSquareClick(i)}
                pieceData={sq}
                selected={i === selectedSquareIndex}
                highlighted={possibleMoves.includes(i)}
                sx={gridStylesMap[i]}
              />
            ))
          : null}
      </BoardGrid>
    </PlayArea>
  );
}

const PlayArea = styled.section`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const InfoRow = styled.div`
  width: calc((100vw / 12 - 5px) * 6);
  max-width: calc((100vw / 12 - 5px) * 6);
  display: grid;
  grid-template-columns: 80px repeat(2, 1fr) 80px;
  grid-template-rows: 50px;
  gap: 20px;
  margin: 16px 0;

  button {
    height: 100%;
    font-size: 20px;
  }
  .placename {
    height: 100%;
  }
`;

const TurnIndicator = styled.div`
  display: grid;
  place-items: center;
  border-radius: 8px;
  font-weight: bold;
  border: 2px solid black;
  text-align: center;
  font-size: 28px;
  height: 100%;

  &.black {
    background-color: black;
    color: white;
  }
`;

const CheckIndicator = styled.div`
  display: grid;
  place-items: center;
  background-color: firebrick;
  color: white;
  font-weight: bold;
  font-size: 28px;
  border-radius: 8px;
  height: 100%;
`;

const BoardGrid = styled.div`
  border: solid 1px black;
  display: grid;
  /* grid-template-columns: repeat(16, calc(100vw / 16 - 5px));
  grid-template-rows: calc(100vw / 16 - 5px); */
  grid-template-columns: repeat(6, calc(100vw / 12 - 5px));
  grid-template-rows: repeat(5, calc(100vw / 12 - 5px));
  background-color: #999;

  &.check {
    border: solid 3px red;
  }
`;

export default Board;
