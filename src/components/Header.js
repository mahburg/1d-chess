import styled from 'styled-components';

function Header() {
  return (
    <Wrapper>
      <h1>1D Chess</h1>
    </Wrapper>
  );
}

const Wrapper = styled.header`
  width: 100%;
  background-color: #333;
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Header;
