import React from 'react';

import Bishop from './pieces/Bishop';
import King from './pieces/King';
import Knight from './pieces/Knight';
import Pawn from './pieces/Pawn';
import Queen from './pieces/Queen';
import Rook from './pieces/Rook';

import { getPieceData } from './util';

const pieceMap = {
  b: Bishop,
  k: King,
  n: Knight,
  p: Pawn,
  q: Queen,
  r: Rook,
};

function Piece({ pieceData }) {
  const { color, piece } = getPieceData(pieceData);
  const SVG = pieceMap[piece];

  return <SVG white={color === 'w'} />;
}

export default Piece;
