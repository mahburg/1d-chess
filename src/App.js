import styled from 'styled-components';
import Board from './components/Board';
import './App.css';
import Footer from './components/Footer';
import Header from './components/Header';

function App() {
  return (
    <Wrapper>
      <Header />
      <Board />
      <Footer />
    </Wrapper>
  );
}

const Wrapper = styled.main`
  width: 100%;
  height: 100vh;
  display: grid;
  grid-template-rows: 60px 1fr auto;
`;

export default App;
